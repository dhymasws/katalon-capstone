<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get Schedule</name>
   <tag></tag>
   <elementGuidId>67e6af73-6337-4e37-8a61-7057242b9886</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5OTczNTMwLCJpYXQiOjE2NzkzNjg3MzAsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.FVjG5jDnCJyxmzPpls5McYQT2K1ErZtgOGTJZVUmI_rnYf2aSSfmzfDI8gT0hFlzJfZVz37WKZibjMI3oQRJtnDxjAh0dyrd4B2txKKddkp5z5-jhzSZLIAD5G3y2obyudYpv2L5SHlzJ4a0_3RsuA6v687SdTzPDS9kuRM5WE0I9d3ZSz1nRtWfSAOUUx2ByI7JkzpAtjDyu5B2WmdwLqzXItgmqBuXTM2X8OnGucJyGHXkTj19_0FWd5LWmdfuEBgMihzjWY47sIVYxGLb97WrZuligQgepkZfcfcBLQbrLSu7fmYNH7BYCwIw6pinlpjLruE-3ZvnKWA2bPrJmA</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5OTczNTMwLCJpYXQiOjE2NzkzNjg3MzAsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.FVjG5jDnCJyxmzPpls5McYQT2K1ErZtgOGTJZVUmI_rnYf2aSSfmzfDI8gT0hFlzJfZVz37WKZibjMI3oQRJtnDxjAh0dyrd4B2txKKddkp5z5-jhzSZLIAD5G3y2obyudYpv2L5SHlzJ4a0_3RsuA6v687SdTzPDS9kuRM5WE0I9d3ZSz1nRtWfSAOUUx2ByI7JkzpAtjDyu5B2WmdwLqzXItgmqBuXTM2X8OnGucJyGHXkTj19_0FWd5LWmdfuEBgMihzjWY47sIVYxGLb97WrZuligQgepkZfcfcBLQbrLSu7fmYNH7BYCwIw6pinlpjLruE-3ZvnKWA2bPrJmA</value>
      <webElementGuid>16c7cd0c-bb6c-49f1-8bd6-1b4b3f1ab763</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/api/workshops/schedules</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>926a0c23-a304-4aaf-b41d-960a9c8d73d8</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
