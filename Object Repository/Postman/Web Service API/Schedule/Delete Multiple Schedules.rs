<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Delete Multiple Schedules</name>
   <tag></tag>
   <elementGuidId>fd8b91d7-bda0-457e-a6d1-1cf72877710b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNSIsInJvbGUiOiJXT1JLU0hPUCIsImlzcyI6Ik1la2FuaUt1IiwiaWQiOjUsImV4cCI6MTY3OTkzNzU5MywiaWF0IjoxNjc5MzMyNzkzLCJlbWFpbCI6IndvcmtzaG9wN0BnbWFpbC5jb20ifQ.xyklpKjmZ4LkHk2DfHH55o3QZEonqSgYn2wchMkOtw9Azg5jLtZGoy_W6sWa50BCohkhB03_L3V3ZM0GQIHQq2IssldYrj9AjYcwI9Momeotc8Y-EYeSBwwNefGoQOLDE2_8kD5HDo06dJSOEUck3LVmfH-XIwrTw5okr3eTAGBzLxWFY6GN9Z7f44g7gOAfNCT0TPPmeBCreEkDwti5HsIGn0U6iQQkFZGnnEH_Jx0dNidKL0_cm7bXVLQ0WqGRO7_fK3UveeN0_HYBkUO7Nm0zYT88DAMvaa9NwvVFftLEEbF8vnI8bK6ket-SgoBgWZM0RbNaf7J1HuJpBuqYBw</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[\n    \&quot;f79f244e-c6d4-4f0f-ab7b-62861bb1940c\&quot;\n]&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>text/plain</value>
      <webElementGuid>33374026-6fb0-4e33-bbb9-cf13e9d07776</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNSIsInJvbGUiOiJXT1JLU0hPUCIsImlzcyI6Ik1la2FuaUt1IiwiaWQiOjUsImV4cCI6MTY3OTkzNzU5MywiaWF0IjoxNjc5MzMyNzkzLCJlbWFpbCI6IndvcmtzaG9wN0BnbWFpbC5jb20ifQ.xyklpKjmZ4LkHk2DfHH55o3QZEonqSgYn2wchMkOtw9Azg5jLtZGoy_W6sWa50BCohkhB03_L3V3ZM0GQIHQq2IssldYrj9AjYcwI9Momeotc8Y-EYeSBwwNefGoQOLDE2_8kD5HDo06dJSOEUck3LVmfH-XIwrTw5okr3eTAGBzLxWFY6GN9Z7f44g7gOAfNCT0TPPmeBCreEkDwti5HsIGn0U6iQQkFZGnnEH_Jx0dNidKL0_cm7bXVLQ0WqGRO7_fK3UveeN0_HYBkUO7Nm0zYT88DAMvaa9NwvVFftLEEbF8vnI8bK6ket-SgoBgWZM0RbNaf7J1HuJpBuqYBw</value>
      <webElementGuid>56f95351-404a-4a48-9224-92f9fa14c939</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>DELETE</restRequestMethod>
   <restUrl>${url}/api/workshops/schedules/multi-del</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>1f0b6dcc-196e-4039-ac3e-6daa2724fc2f</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
