<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Add Schedule (once)</name>
   <tag></tag>
   <elementGuidId>7ed3e807-0a07-4848-927c-f75c968b0c30</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNSIsInJvbGUiOiJXT1JLU0hPUCIsImlzcyI6Ik1la2FuaUt1IiwiaWQiOjUsImV4cCI6MTY3OTkzNzU5MywiaWF0IjoxNjc5MzMyNzkzLCJlbWFpbCI6IndvcmtzaG9wN0BnbWFpbC5jb20ifQ.xyklpKjmZ4LkHk2DfHH55o3QZEonqSgYn2wchMkOtw9Azg5jLtZGoy_W6sWa50BCohkhB03_L3V3ZM0GQIHQq2IssldYrj9AjYcwI9Momeotc8Y-EYeSBwwNefGoQOLDE2_8kD5HDo06dJSOEUck3LVmfH-XIwrTw5okr3eTAGBzLxWFY6GN9Z7f44g7gOAfNCT0TPPmeBCreEkDwti5HsIGn0U6iQQkFZGnnEH_Jx0dNidKL0_cm7bXVLQ0WqGRO7_fK3UveeN0_HYBkUO7Nm0zYT88DAMvaa9NwvVFftLEEbF8vnI8bK6ket-SgoBgWZM0RbNaf7J1HuJpBuqYBw</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;forDate\&quot;: \&quot;2017-10-10\&quot;,\n    \&quot;status\&quot;: \&quot;HALF_DAY\&quot;,\n    \&quot;openTime\&quot;: \&quot;09:00\&quot;,\n    \&quot;closeTime\&quot;: \&quot;12:00\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>566d3061-b8e6-4282-a01e-caa4f8ef8b39</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNSIsInJvbGUiOiJXT1JLU0hPUCIsImlzcyI6Ik1la2FuaUt1IiwiaWQiOjUsImV4cCI6MTY3OTkzNzU5MywiaWF0IjoxNjc5MzMyNzkzLCJlbWFpbCI6IndvcmtzaG9wN0BnbWFpbC5jb20ifQ.xyklpKjmZ4LkHk2DfHH55o3QZEonqSgYn2wchMkOtw9Azg5jLtZGoy_W6sWa50BCohkhB03_L3V3ZM0GQIHQq2IssldYrj9AjYcwI9Momeotc8Y-EYeSBwwNefGoQOLDE2_8kD5HDo06dJSOEUck3LVmfH-XIwrTw5okr3eTAGBzLxWFY6GN9Z7f44g7gOAfNCT0TPPmeBCreEkDwti5HsIGn0U6iQQkFZGnnEH_Jx0dNidKL0_cm7bXVLQ0WqGRO7_fK3UveeN0_HYBkUO7Nm0zYT88DAMvaa9NwvVFftLEEbF8vnI8bK6ket-SgoBgWZM0RbNaf7J1HuJpBuqYBw</value>
      <webElementGuid>11bea94c-69fe-430a-bb62-7b9443b7f94f</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${url}/api/workshops/43/schedules/</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>d02eec6a-3514-4ab6-8ac8-17da44797ac2</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.merchant_id</defaultValue>
      <description></description>
      <id>c8631e6a-1d0f-4baf-a347-9bfa965a1a2b</id>
      <masked>false</masked>
      <name>merchant_id</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
