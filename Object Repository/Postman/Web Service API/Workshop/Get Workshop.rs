<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get Workshop</name>
   <tag></tag>
   <elementGuidId>e4ff9218-aeee-4cbb-bfbb-ab1023101298</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5ODgzMzE1LCJpYXQiOjE2NzkyNzg1MTUsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.0X-3xayI9TEVyuQ2Z4JGsi5A-xeocOG8d8NCOk5K2SwMfbCWZx0hf8Lzijs_EfuRSij2tnBAMTFVffGSTlzK4pEcwXfFS1pdUukV4mcAOFj2ldRLYAj0_3xVWcJwnFDHY2wd0zCmqwwKvkhhaDr9ULeeecAjvqU91eN4JkZmuYNN3J3aoL26smf0zqm9mB4bx3SwRhSSf7sn876Io1fFS6QNe2fQGgdvgYaTwfjKs5rn-gzvYqmmqc8vc2TEioomK9vbX2wu0QLw-BJ7PfaKIusZ-R9p0UvheRvPVsOwV18pl94ErdbH36Dq7G1MTtI4oWPSRIEdHTIU8Z_mGMHz3g</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5ODgzMzE1LCJpYXQiOjE2NzkyNzg1MTUsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.0X-3xayI9TEVyuQ2Z4JGsi5A-xeocOG8d8NCOk5K2SwMfbCWZx0hf8Lzijs_EfuRSij2tnBAMTFVffGSTlzK4pEcwXfFS1pdUukV4mcAOFj2ldRLYAj0_3xVWcJwnFDHY2wd0zCmqwwKvkhhaDr9ULeeecAjvqU91eN4JkZmuYNN3J3aoL26smf0zqm9mB4bx3SwRhSSf7sn876Io1fFS6QNe2fQGgdvgYaTwfjKs5rn-gzvYqmmqc8vc2TEioomK9vbX2wu0QLw-BJ7PfaKIusZ-R9p0UvheRvPVsOwV18pl94ErdbH36Dq7G1MTtI4oWPSRIEdHTIU8Z_mGMHz3g</value>
      <webElementGuid>06d1f7a2-a623-4cef-8cce-14a97ddd0be1</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/api/workshops/43</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>9bb25d7d-9278-4397-bbc6-023d72bd912a</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
