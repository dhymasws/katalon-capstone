<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Create Workshop</name>
   <tag></tag>
   <elementGuidId>8adc8500-7aa4-4ed5-b590-288d86074a73</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTQiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1NCwiZXhwIjoxNjc5OTc2NjM3LCJpYXQiOjE2NzkzNzE4MzcsImVtYWlsIjoid29ya3Nob3AyMUBnbWFpbC5jb20ifQ.4a_0aS6Sy3Lpx4WwE6SH841ZYt-Dz-AMUVJVHH3NWN02ax1JZNT3tdiZDJeb4kSs53suhhMjZMeAGLC8M5TyiYaucnOIxtG2XTv7ywDngvtKiN5BzXIvkUwUib-bDpulOY8rBJeqGi3PS-b-XdKJmelIytx1QsYmsGpzj8rVmwmKBIuDY2HWzrR1WuCY9qs4LhRJQT5jXQ1AE9EGlK5uaOWuqwQBnWeJXY8q5e9T1Rw6f8NHqiYIa42uLqEGSXcPjx1fwy059zXqqOnibbyhlKVmPY_oa1xaGa46ArQ-95AbUHtYtK0qheWXWDYzft34PsOarTAoz2tVHp9zAGqv3g</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;name\&quot;:\&quot;NSS Slipi 2\&quot;,\n    \&quot;description\&quot;:\&quot;Nusantara Sakti Slipi petamburan\&quot;,\n    \&quot;address\&quot;:\&quot;Slipi petamburan\&quot;,\n    \&quot;latitude\&quot;:1421251.0,\n    \&quot;longitude\&quot;:1874381.0,\n    \&quot;motorcycle\&quot;:true,\n    \&quot;car\&quot;:false,\n    \&quot;openTime\&quot;: \&quot;08:00\&quot;,\n    \&quot;closeTime\&quot;: \&quot;17:00\&quot;,\n    \&quot;closeDay\&quot;: [1,2,3,4,5]\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>c65f06a3-1fbe-4998-9b5b-4cfa0f1c7338</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTQiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1NCwiZXhwIjoxNjc5OTc2NjM3LCJpYXQiOjE2NzkzNzE4MzcsImVtYWlsIjoid29ya3Nob3AyMUBnbWFpbC5jb20ifQ.4a_0aS6Sy3Lpx4WwE6SH841ZYt-Dz-AMUVJVHH3NWN02ax1JZNT3tdiZDJeb4kSs53suhhMjZMeAGLC8M5TyiYaucnOIxtG2XTv7ywDngvtKiN5BzXIvkUwUib-bDpulOY8rBJeqGi3PS-b-XdKJmelIytx1QsYmsGpzj8rVmwmKBIuDY2HWzrR1WuCY9qs4LhRJQT5jXQ1AE9EGlK5uaOWuqwQBnWeJXY8q5e9T1Rw6f8NHqiYIa42uLqEGSXcPjx1fwy059zXqqOnibbyhlKVmPY_oa1xaGa46ArQ-95AbUHtYtK0qheWXWDYzft34PsOarTAoz2tVHp9zAGqv3g</value>
      <webElementGuid>b984e68a-5065-4554-8bdc-928367ef2c43</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${url}/api/workshops</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>c9fddda4-333b-4f8a-a655-a1a65c10c380</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
