<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get All Workshop</name>
   <tag></tag>
   <elementGuidId>d39f0324-1ecd-41a5-83e4-fa855434a528</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiMSIsInJvbGUiOiJTVVBFUkFETUlOIiwiaXNzIjoiTWVrYW5pS3UiLCJpZCI6MSwiZXhwIjoxNjc5ODg0MDY0LCJpYXQiOjE2NzkyNzkyNjQsImVtYWlsIjoic3VwZXJhZG1pbkBlbWFpbC5leGFtcGxlIn0.egTnynXJ9Ef6yJBa3iPrpyc3nGnozGwbwmJdz-a0A6DRxQyxyv5Xj0Zt9wNDgdXpSh1ARa7EFifjnaI3iC5jRDAKvnqK5jLv3g2U2HILaUztRAiT_L0NLDv7-EthRhvAy3ox2bGJg6g-v_rcC8TMba13X3lyUcw2Eo3N5NZPau90WW2ylyJhuK4KQ1nPb8GgdtLxA1ECBTjf4XIXBZY-2bqAvQu0XIUpTxnu89iZgoo_gURa-4h736dnEoaWOlFIXjSPl_N03YepIriUSIXgVXspf9jxKtIq3RT713YdUnKfiMQVekSOchXLBCIiURmLLxXSv_WYdJu0MSjvhX3G9w</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiMSIsInJvbGUiOiJTVVBFUkFETUlOIiwiaXNzIjoiTWVrYW5pS3UiLCJpZCI6MSwiZXhwIjoxNjc5ODg0MDY0LCJpYXQiOjE2NzkyNzkyNjQsImVtYWlsIjoic3VwZXJhZG1pbkBlbWFpbC5leGFtcGxlIn0.egTnynXJ9Ef6yJBa3iPrpyc3nGnozGwbwmJdz-a0A6DRxQyxyv5Xj0Zt9wNDgdXpSh1ARa7EFifjnaI3iC5jRDAKvnqK5jLv3g2U2HILaUztRAiT_L0NLDv7-EthRhvAy3ox2bGJg6g-v_rcC8TMba13X3lyUcw2Eo3N5NZPau90WW2ylyJhuK4KQ1nPb8GgdtLxA1ECBTjf4XIXBZY-2bqAvQu0XIUpTxnu89iZgoo_gURa-4h736dnEoaWOlFIXjSPl_N03YepIriUSIXgVXspf9jxKtIq3RT713YdUnKfiMQVekSOchXLBCIiURmLLxXSv_WYdJu0MSjvhX3G9w</value>
      <webElementGuid>87dc2420-4dad-4664-b8cc-188187d8c151</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/api/workshops?limit=5&amp;page=0</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>04283a36-8041-41ed-b0d9-01d6570bd898</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
