<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Change Password</name>
   <tag></tag>
   <elementGuidId>2a5ec53a-4f7a-4e42-bf35-fd6e7e0d44fb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5ODgzMzE1LCJpYXQiOjE2NzkyNzg1MTUsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.0X-3xayI9TEVyuQ2Z4JGsi5A-xeocOG8d8NCOk5K2SwMfbCWZx0hf8Lzijs_EfuRSij2tnBAMTFVffGSTlzK4pEcwXfFS1pdUukV4mcAOFj2ldRLYAj0_3xVWcJwnFDHY2wd0zCmqwwKvkhhaDr9ULeeecAjvqU91eN4JkZmuYNN3J3aoL26smf0zqm9mB4bx3SwRhSSf7sn876Io1fFS6QNe2fQGgdvgYaTwfjKs5rn-gzvYqmmqc8vc2TEioomK9vbX2wu0QLw-BJ7PfaKIusZ-R9p0UvheRvPVsOwV18pl94ErdbH36Dq7G1MTtI4oWPSRIEdHTIU8Z_mGMHz3g</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;oldPassword\&quot;: \&quot;Doku@123\&quot;,\n    \&quot;newPassword\&quot;: \&quot;Doku@123\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>39bbe041-923c-4c27-a420-e8784f5a97d3</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5ODgzMzE1LCJpYXQiOjE2NzkyNzg1MTUsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.0X-3xayI9TEVyuQ2Z4JGsi5A-xeocOG8d8NCOk5K2SwMfbCWZx0hf8Lzijs_EfuRSij2tnBAMTFVffGSTlzK4pEcwXfFS1pdUukV4mcAOFj2ldRLYAj0_3xVWcJwnFDHY2wd0zCmqwwKvkhhaDr9ULeeecAjvqU91eN4JkZmuYNN3J3aoL26smf0zqm9mB4bx3SwRhSSf7sn876Io1fFS6QNe2fQGgdvgYaTwfjKs5rn-gzvYqmmqc8vc2TEioomK9vbX2wu0QLw-BJ7PfaKIusZ-R9p0UvheRvPVsOwV18pl94ErdbH36Dq7G1MTtI4oWPSRIEdHTIU8Z_mGMHz3g</value>
      <webElementGuid>c49e9e1e-59d7-422f-8cf0-74d24f587f29</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PATCH</restRequestMethod>
   <restUrl>${url}/api/auth/password/53</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>8bf7bda8-924f-41a6-a3d7-beda73d29bf3</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.merchant_id</defaultValue>
      <description></description>
      <id>e53d4a75-a10b-46ff-9868-7ab93347ca94</id>
      <masked>false</masked>
      <name>merchant_id</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
