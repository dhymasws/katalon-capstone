<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Delete One Product</name>
   <tag></tag>
   <elementGuidId>b1e9be2b-0306-4b8b-b7ff-2d3c44955afa</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNSIsInJvbGUiOiJXT1JLU0hPUCIsImlzcyI6Ik1la2FuaUt1IiwiaWQiOjUsImV4cCI6MTY3OTkzNzU5MywiaWF0IjoxNjc5MzMyNzkzLCJlbWFpbCI6IndvcmtzaG9wN0BnbWFpbC5jb20ifQ.xyklpKjmZ4LkHk2DfHH55o3QZEonqSgYn2wchMkOtw9Azg5jLtZGoy_W6sWa50BCohkhB03_L3V3ZM0GQIHQq2IssldYrj9AjYcwI9Momeotc8Y-EYeSBwwNefGoQOLDE2_8kD5HDo06dJSOEUck3LVmfH-XIwrTw5okr3eTAGBzLxWFY6GN9Z7f44g7gOAfNCT0TPPmeBCreEkDwti5HsIGn0U6iQQkFZGnnEH_Jx0dNidKL0_cm7bXVLQ0WqGRO7_fK3UveeN0_HYBkUO7Nm0zYT88DAMvaa9NwvVFftLEEbF8vnI8bK6ket-SgoBgWZM0RbNaf7J1HuJpBuqYBw</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNSIsInJvbGUiOiJXT1JLU0hPUCIsImlzcyI6Ik1la2FuaUt1IiwiaWQiOjUsImV4cCI6MTY3OTkzNzU5MywiaWF0IjoxNjc5MzMyNzkzLCJlbWFpbCI6IndvcmtzaG9wN0BnbWFpbC5jb20ifQ.xyklpKjmZ4LkHk2DfHH55o3QZEonqSgYn2wchMkOtw9Azg5jLtZGoy_W6sWa50BCohkhB03_L3V3ZM0GQIHQq2IssldYrj9AjYcwI9Momeotc8Y-EYeSBwwNefGoQOLDE2_8kD5HDo06dJSOEUck3LVmfH-XIwrTw5okr3eTAGBzLxWFY6GN9Z7f44g7gOAfNCT0TPPmeBCreEkDwti5HsIGn0U6iQQkFZGnnEH_Jx0dNidKL0_cm7bXVLQ0WqGRO7_fK3UveeN0_HYBkUO7Nm0zYT88DAMvaa9NwvVFftLEEbF8vnI8bK6ket-SgoBgWZM0RbNaf7J1HuJpBuqYBw</value>
      <webElementGuid>f222455e-b77e-4d04-8d35-f61adaff3b9d</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>DELETE</restRequestMethod>
   <restUrl>${url}/api/products/6ba44851-1338-43ae-9575-375cc9c68734</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>f50ba1ed-6d25-4732-aa66-c1c59f0e5a62</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
