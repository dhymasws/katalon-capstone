<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get Products</name>
   <tag></tag>
   <elementGuidId>82e2f5bf-d4a2-4d66-a537-eb3813762ab5</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5ODgzMzE1LCJpYXQiOjE2NzkyNzg1MTUsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.0X-3xayI9TEVyuQ2Z4JGsi5A-xeocOG8d8NCOk5K2SwMfbCWZx0hf8Lzijs_EfuRSij2tnBAMTFVffGSTlzK4pEcwXfFS1pdUukV4mcAOFj2ldRLYAj0_3xVWcJwnFDHY2wd0zCmqwwKvkhhaDr9ULeeecAjvqU91eN4JkZmuYNN3J3aoL26smf0zqm9mB4bx3SwRhSSf7sn876Io1fFS6QNe2fQGgdvgYaTwfjKs5rn-gzvYqmmqc8vc2TEioomK9vbX2wu0QLw-BJ7PfaKIusZ-R9p0UvheRvPVsOwV18pl94ErdbH36Dq7G1MTtI4oWPSRIEdHTIU8Z_mGMHz3g</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5ODgzMzE1LCJpYXQiOjE2NzkyNzg1MTUsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.0X-3xayI9TEVyuQ2Z4JGsi5A-xeocOG8d8NCOk5K2SwMfbCWZx0hf8Lzijs_EfuRSij2tnBAMTFVffGSTlzK4pEcwXfFS1pdUukV4mcAOFj2ldRLYAj0_3xVWcJwnFDHY2wd0zCmqwwKvkhhaDr9ULeeecAjvqU91eN4JkZmuYNN3J3aoL26smf0zqm9mB4bx3SwRhSSf7sn876Io1fFS6QNe2fQGgdvgYaTwfjKs5rn-gzvYqmmqc8vc2TEioomK9vbX2wu0QLw-BJ7PfaKIusZ-R9p0UvheRvPVsOwV18pl94ErdbH36Dq7G1MTtI4oWPSRIEdHTIU8Z_mGMHz3g</value>
      <webElementGuid>e7f94242-8c2e-4f3d-b62e-464815929819</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/api/products/</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>cdf69b91-cef2-45de-9c19-ee821682983f</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
