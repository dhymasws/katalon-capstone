<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Product</name>
   <tag></tag>
   <elementGuidId>1040f83a-753d-4d2a-babe-9de5845f1554</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5ODgzMzE1LCJpYXQiOjE2NzkyNzg1MTUsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.0X-3xayI9TEVyuQ2Z4JGsi5A-xeocOG8d8NCOk5K2SwMfbCWZx0hf8Lzijs_EfuRSij2tnBAMTFVffGSTlzK4pEcwXfFS1pdUukV4mcAOFj2ldRLYAj0_3xVWcJwnFDHY2wd0zCmqwwKvkhhaDr9ULeeecAjvqU91eN4JkZmuYNN3J3aoL26smf0zqm9mB4bx3SwRhSSf7sn876Io1fFS6QNe2fQGgdvgYaTwfjKs5rn-gzvYqmmqc8vc2TEioomK9vbX2wu0QLw-BJ7PfaKIusZ-R9p0UvheRvPVsOwV18pl94ErdbH36Dq7G1MTtI4oWPSRIEdHTIU8Z_mGMHz3g</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;name\&quot;:\&quot;Oli gardan\&quot;,\n    \&quot;price\&quot;:\&quot;50000\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>00d5cc2e-1b78-4f4e-a611-5585a65f070a</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNTMiLCJyb2xlIjoiV09SS1NIT1AiLCJpc3MiOiJNZWthbmlLdSIsImlkIjo1MywiZXhwIjoxNjc5ODgzMzE1LCJpYXQiOjE2NzkyNzg1MTUsImVtYWlsIjoid29ya3Nob3A3QGdtYWlsLmNvbSJ9.0X-3xayI9TEVyuQ2Z4JGsi5A-xeocOG8d8NCOk5K2SwMfbCWZx0hf8Lzijs_EfuRSij2tnBAMTFVffGSTlzK4pEcwXfFS1pdUukV4mcAOFj2ldRLYAj0_3xVWcJwnFDHY2wd0zCmqwwKvkhhaDr9ULeeecAjvqU91eN4JkZmuYNN3J3aoL26smf0zqm9mB4bx3SwRhSSf7sn876Io1fFS6QNe2fQGgdvgYaTwfjKs5rn-gzvYqmmqc8vc2TEioomK9vbX2wu0QLw-BJ7PfaKIusZ-R9p0UvheRvPVsOwV18pl94ErdbH36Dq7G1MTtI4oWPSRIEdHTIU8Z_mGMHz3g</value>
      <webElementGuid>4d7eb20c-eb36-4f09-bfbe-d777ae100bac</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PATCH</restRequestMethod>
   <restUrl>${url}/api/products/119abe7b-3e7c-4d1a-b2ff-29db4dc28b97</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>d81234e0-27eb-4338-932c-8293c1bba1b0</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
