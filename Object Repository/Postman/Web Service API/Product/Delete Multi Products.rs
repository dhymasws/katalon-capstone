<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Delete Multi Products</name>
   <tag></tag>
   <elementGuidId>7c45cd40-7c80-4e07-9c77-1d7cd2aff62f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNSIsInJvbGUiOiJXT1JLU0hPUCIsImlzcyI6Ik1la2FuaUt1IiwiaWQiOjUsImV4cCI6MTY3OTkzNzU5MywiaWF0IjoxNjc5MzMyNzkzLCJlbWFpbCI6IndvcmtzaG9wN0BnbWFpbC5jb20ifQ.xyklpKjmZ4LkHk2DfHH55o3QZEonqSgYn2wchMkOtw9Azg5jLtZGoy_W6sWa50BCohkhB03_L3V3ZM0GQIHQq2IssldYrj9AjYcwI9Momeotc8Y-EYeSBwwNefGoQOLDE2_8kD5HDo06dJSOEUck3LVmfH-XIwrTw5okr3eTAGBzLxWFY6GN9Z7f44g7gOAfNCT0TPPmeBCreEkDwti5HsIGn0U6iQQkFZGnnEH_Jx0dNidKL0_cm7bXVLQ0WqGRO7_fK3UveeN0_HYBkUO7Nm0zYT88DAMvaa9NwvVFftLEEbF8vnI8bK6ket-SgoBgWZM0RbNaf7J1HuJpBuqYBw</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[\n \&quot;4408304f-7c2b-45bd-91de-b8908f52d605\&quot;\n]&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>bee79f4f-97ce-41b9-97df-db3ab90cc486</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdXNwZW5kIjpmYWxzZSwic3ViIjoiNSIsInJvbGUiOiJXT1JLU0hPUCIsImlzcyI6Ik1la2FuaUt1IiwiaWQiOjUsImV4cCI6MTY3OTkzNzU5MywiaWF0IjoxNjc5MzMyNzkzLCJlbWFpbCI6IndvcmtzaG9wN0BnbWFpbC5jb20ifQ.xyklpKjmZ4LkHk2DfHH55o3QZEonqSgYn2wchMkOtw9Azg5jLtZGoy_W6sWa50BCohkhB03_L3V3ZM0GQIHQq2IssldYrj9AjYcwI9Momeotc8Y-EYeSBwwNefGoQOLDE2_8kD5HDo06dJSOEUck3LVmfH-XIwrTw5okr3eTAGBzLxWFY6GN9Z7f44g7gOAfNCT0TPPmeBCreEkDwti5HsIGn0U6iQQkFZGnnEH_Jx0dNidKL0_cm7bXVLQ0WqGRO7_fK3UveeN0_HYBkUO7Nm0zYT88DAMvaa9NwvVFftLEEbF8vnI8bK6ket-SgoBgWZM0RbNaf7J1HuJpBuqYBw</value>
      <webElementGuid>cd27f843-b5bb-41b1-aa09-389acb550e17</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.5.5</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>DELETE</restRequestMethod>
   <restUrl>${url}/api/products/delete-products</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
