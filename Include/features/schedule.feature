#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@schedule
Feature: schedule
  I want CRUD schedule

  @schedule-create
  Scenario: I want to create schdule
    When 	I send create schedule request
    Then  I verify the schedule
      	
  
  @schedule-get
  Scenario: After login,  i want to see my profile account
  	When I send get schedule request
  	Then I verify all schedule 
  
  @product-update-time
  Scenario: After login,  i want change description
  	When I send update time schedule request
  	Then I verify schedule updated
  	
  @product-update-day
  Scenario: After login,  i want chation
  	When I send update close day schedule request
  	Then I verify close day schedule updated
  	
  @schedule-delete
  Scenario: After login, i want delete product
  	When I send request to delete schedule
  	Then I verify one schedule deleted
  	
  @schdedule-delete-multi
  Scenario: After login, i want delete multi product
  	When I send request delete multi schedule
  	Then I verify multi schedule deleted