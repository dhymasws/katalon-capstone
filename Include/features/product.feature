#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@product
Feature: product
  I want login with my account

  @product-create
  Scenario: I want create product
    When 	I send create product request
    Then  I verify the product
      	
  
  @product-get
  Scenario: After login,  i want to see my profile account
  	When I send get product request
  	Then I verify all product 
  
  @product-update
  Scenario: After login,  i want change description
  	When I send update product request
  	Then I verify product updated
  	
  @product-delete
  Scenario: After login, i want delete product
  	When I send request to delete product
  	Then I verify one product deleted
  	
  @product-delete-multi
  Scenario: After login, i want delete multi product
  	When I send request to delete multi product
  	Then I verify multi product deleted