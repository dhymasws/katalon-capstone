#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@auth
Feature: login
  I want login with my account

  @auth-login
  Scenario: I want login with my account
    When 	I send login request 
    Then  I verify to login
    
  
  @auth-register
  Scenario: I want to register
   	When I send register request
   	Then I verify to register
   	
  
  @auth-get-user-info
  Scenario: After login,  i want to see my profile account
  	When I send check info request
  	Then I see my profile
  	
  @auth-get-all-user
  Scenario: After login (As superadmin),  i want to see my profile account
  	When I send check all user request
  	Then I see all user
  	
  @auth-edit-status-user
  Scenario: After login, i want change password
  	When I send change password request
  	Then I change password
