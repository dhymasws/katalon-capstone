#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@workshop
Feature: workshop
  I want login with my account

  @workshop-create
  Scenario: I want register workshop
    When 	I send register workshop request 
    Then  I verify workshop was registered
    
  
  @workshop-get-all
  Scenario: I want to see all workshop
   	When I send request get all  
   	Then I see all workshop
   	
  
  @workshop-get
  Scenario: After login,  i want to see my profile account
  	When I send get request workshop
  	Then I see workshop my profile
  
  @workshop-edit-desc
  Scenario: After login,  i want change description
  	When I send edit desc request
  	Then I verify description
  	
  @workshop-edit-profile
  Scenario: After login, i want change workshop profile
  	When I send change profile request
  	Then I verify profile